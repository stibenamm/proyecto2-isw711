﻿using System.Web;
using System.Web.Mvc;

namespace proyecto2_ISW711
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}