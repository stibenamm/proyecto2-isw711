﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto2_ISW711.Models
{
    public class Red_Social
    {
        public int Id { get; set; }
        public string Icono { get; set; }
        public string Link { get; set; }
    }
}