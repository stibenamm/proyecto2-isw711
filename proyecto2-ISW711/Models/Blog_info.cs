﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto2_ISW711.Models
{
    public class Blog_info
    {
        public string Autor { get; set; }
        public string Biografia { get; set; }
        public string NombreBlog { get; set; }
        public string Detalle { get; set; }
        public virtual IEnumerable<Red_Social> Redes_Sociales { get; set; }
    }
}