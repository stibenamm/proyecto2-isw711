﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace proyecto2_ISW711.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public DateTime Fecha { get; set; }
        public string Contenido { get; set; }
        public virtual ICollection< Comentario> Comentario { get; set; }
    }
}