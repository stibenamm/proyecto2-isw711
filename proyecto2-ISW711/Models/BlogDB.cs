﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace proyecto2_ISW711.Models
{
    public class BlogDB: DbContext
    {
        public BlogDB() : base("DB16")
        {

        }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Red_Social> Redes_Sociales { get; set; }
    }
}