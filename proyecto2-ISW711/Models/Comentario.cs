﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace proyecto2_ISW711.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public string Contacto { get; set; }
        public string Correo { get; set; }
        public string Texto { get; set; }
        public bool Activo { get; set; }
        public int PostId { get; set; }
    }
}