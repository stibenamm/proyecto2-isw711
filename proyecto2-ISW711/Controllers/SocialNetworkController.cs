﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using proyecto2_ISW711.Models;

namespace proyecto2_ISW711.Controllers
{
    public class SocialNetworkController : Controller
    {
        private BlogDB db = new BlogDB();

        //
        // GET: /SocialNetwork/

        public ActionResult Index()
        {
            return View(db.Redes_Sociales.ToList());
        }

        //
        // GET: /SocialNetwork/Details/5

        public ActionResult Details(int id = 0)
        {
            Red_Social red_social = db.Redes_Sociales.Find(id);
            if (red_social == null)
            {
                return HttpNotFound();
            }
            return View(red_social);
        }

        //
        // GET: /SocialNetwork/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SocialNetwork/Create

        [HttpPost]
        public ActionResult Create(Red_Social red_social)
        {
            if (ModelState.IsValid)
            {
                db.Redes_Sociales.Add(red_social);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(red_social);
        }

        //
        // GET: /SocialNetwork/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Red_Social red_social = db.Redes_Sociales.Find(id);
            if (red_social == null)
            {
                return HttpNotFound();
            }
            return View(red_social);
        }

        //
        // POST: /SocialNetwork/Edit/5

        [HttpPost]
        public ActionResult Edit(Red_Social red_social)
        {
            if (ModelState.IsValid)
            {
                db.Entry(red_social).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(red_social);
        }

        //
        // GET: /SocialNetwork/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Red_Social red_social = db.Redes_Sociales.Find(id);
            if (red_social == null)
            {
                return HttpNotFound();
            }
            return View(red_social);
        }

        //
        // POST: /SocialNetwork/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Red_Social red_social = db.Redes_Sociales.Find(id);
            db.Redes_Sociales.Remove(red_social);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}