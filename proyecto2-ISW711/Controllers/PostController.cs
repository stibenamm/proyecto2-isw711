﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using proyecto2_ISW711.Models;
using PagedList;
using PagedList.Mvc;
using System.Configuration;
using System.Net.Mail;

namespace proyecto2_ISW711.Controllers
{

    public class PostController : Controller
    {

        private BlogDB db = new BlogDB();


        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index(int page=1)
        {
            List<Post> model = db.Posts.OrderByDescending(post => post.Fecha).ToList();
            int pageSize = 5;

            ViewBag.info = info();

            return View(model.ToPagedList(page, pageSize));
        }

        //[Authorize]
        public ActionResult IndexAdmin()
        {
            return View(db.Posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);

            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.info = info();

            return View(post);
        }

        //
        // GET: /Post/Create
        //[Authorize]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Post post)
        {

            post.Fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Posts.Add(post);
                db.SaveChanges();
            }

            return Redirect("/admin");
        }

        //
        // GET: /Post/Edit/5
        //[Authorize]
        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect("/admin");
            }
            return Redirect("/admin");
        }

        //
        // GET: /Post/Delete/5
        //[Authorize]
        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5
        //[Authorize]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return Redirect("/admin");
        }

        private Blog_info info() 
        {

            Blog_info info = new Blog_info();
            info.Autor = ConfigurationManager.AppSettings["Autor"];
            info.Biografia = ConfigurationManager.AppSettings["Biografia"];
            info.NombreBlog = ConfigurationManager.AppSettings["NombreBlog"];
            info.Detalle = ConfigurationManager.AppSettings["Detalle"];

            List<Red_Social> redes = db.Redes_Sociales.ToList();
            info.Redes_Sociales = redes;

            return info;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}