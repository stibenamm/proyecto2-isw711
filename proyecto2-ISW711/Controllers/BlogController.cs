﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using proyecto2_ISW711.Models;
using System.Configuration;

namespace proyecto2_ISW711.Controllers
{
    
    public class BlogController : Controller
    {
        private BlogDB db = new BlogDB();
        //
        // GET: /Default1/

        public ActionResult Index()
        {
            Blog_info info = new Blog_info();
            info.Autor = ConfigurationManager.AppSettings["Autor"];
            info.Biografia = ConfigurationManager.AppSettings["Biografia"];
            info.NombreBlog = ConfigurationManager.AppSettings["NombreBlog"];
            info.Detalle = ConfigurationManager.AppSettings["Detalle"];

            List<Red_Social> redes = db.Redes_Sociales.ToList();
            info.Redes_Sociales = redes;

            ViewBag.info = info;
            return View(ViewBag.info);
        }

        public ActionResult Edit(string Autor,string Biografia,string NombreBlog, string Detalle)
        {
            ConfigurationManager.AppSettings.Set("Autor", Autor);
            ConfigurationManager.AppSettings.Set("Biografia",Biografia);
            ConfigurationManager.AppSettings.Set("NombreBlog", NombreBlog);
            ConfigurationManager.AppSettings.Set("Detalle", Detalle );

            return RedirectToAction("Index", "Blog");
        }

        [HttpPost]
        public ActionResult AddSocialNetwork(Red_Social nueva) 
        {

            if (ModelState.IsValid)
            {
                db.Redes_Sociales.Add(nueva);
                db.SaveChanges();
            }

            return RedirectToAction("Index", "Blog");
        }









        //
        // POST: /Default1/Create
        /*
        [HttpPost]
        public ActionResult Create(Blog_info blog_info)
        {
            if (ModelState.IsValid)
            {
                db.Blog_info.Add(blog_info);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(blog_info);
        }

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit()
        {
            Red_Social Redes_Sociales = new Red_Social();

            Blog_info blog_info = db.Redes_Sociales.;
            if (blog_info == null)
            {
                return HttpNotFound();
            }
            return View(blog_info);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        public ActionResult Edit(Blog_info blog_info)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog_info).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blog_info);
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Blog_info blog_info = db.Blog_info.Find(id);
            if (blog_info == null)
            {
                return HttpNotFound();
            }
            return View(blog_info);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Blog_info blog_info = db.Blog_info.Find(id);
            db.Blog_info.Remove(blog_info);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }*/
    }
}