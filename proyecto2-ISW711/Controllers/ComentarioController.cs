﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using proyecto2_ISW711.Models;

namespace proyecto2_ISW711.Controllers
{
    public class ComentarioController : Controller
    {
        private BlogDB db = new BlogDB();

       // [Authorize]
        public ActionResult Index()
        {
            return View(db.Comentarios.ToList());
        }

        //[Authorize]
        [HttpPost]
        public ActionResult Create(Comentario comentario)
        {
            if (ModelState.IsValid)
            {
                db.Comentarios.Add(comentario);
                db.SaveChanges();
                SendMail(comentario.Contacto);
            }
            return Redirect("/post/Details/"+ comentario.PostId);
        }

        //[Authorize]
        public ActionResult Edit(int id = 0)
        {
            Comentario comentario = db.Comentarios.Find(id);
            if (comentario == null)
            {
                return HttpNotFound();
            }
            return View(comentario);
        }

        //[Authorize]
        [HttpPost]
        public ActionResult Edit(Comentario comentario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comentario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(comentario);
        }

        //[Authorize]
        public ActionResult Delete(int id = 0)
        {
            Comentario comentario = db.Comentarios.Find(id);
            if (comentario == null)
            {
                return HttpNotFound();
            }
            return View(comentario);
        }

        //[Authorize]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Comentario comentario = db.Comentarios.Find(id);
            db.Comentarios.Remove(comentario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public void SendMail(string contacto)
        {

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.mailgun.org");
            mail.From = new System.Net.Mail.MailAddress("Blog@info.com", "stiben", System.Text.Encoding.UTF8);
            mail.Subject = "Blog";
            mail.Body = "Recibió un nuevo mensaje de " contacto;
            mail.To.Add("stibenamm@gmail.com");

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("postmaster@app3117edd616124713b5fc600e93eb2a70.mailgun.org", "5mxkzw7m1tf8");
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}